# Uni demo API

## Guidelines

The aim of this document is to present to you a simple aPI that you can use as a starting point in you Spring Boot API development.

This document borrows information from:
* [Spring Boot - Building an Application with Spring Boot](https://spring.io/guides/gs/spring-boot/)
* [Spring Boot - Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Spring Boot - Spring Boot with Docker](https://spring.io/guides/gs/spring-boot-docker/)

 
## Docker

In order to download the Docker image you can use the following CMD commands:

* Pull the image:
```
   docker pull mimitheone/uni
```
* Run the image
```
docker run -p 8080:8081 springio/uni-demo-docker
```

⚠ Bear in mind that you need to change the port based on your configuration. The port used within Docker is 8081 if on your port 8080 a different application is running you should change it to any free port. 


## Build it on your own

If you prefer to build the application on your own you should first pull the project from the here and open it with your favourite IDE.
The next step is to build it using your Maven and then you can run it using your Docker installation.

```
mvnw package && java -jar target/uni-demo-docker-0.1.0.jar
docker build -t springio/uni-demo-docker . 
docker run -p 8080:8081 springio/uni-demo-docker
```

## API

* Returns all employees: GET /employees
* Creates an employee: POST /employees
* Returns all information about an employee based on ID: GET /employees/{id}
* Updates an employee based on a given ID: PUT /employees/{id}
* Deletes an employee based on a given ID: DELETE /employees/{id}

## Sample JSON
```
{
    "id": 5,
    "name": "Mickey the mouse",
    "role": "Splendid actor"
}
```

## Contributing

🔥 Pull requests are welcome.

No tests are implemented since the ain is to show how quickly we can start working productively using Spring Boot.
If you want to learn more please follow the links:
* [Spring Boot Tutorial for Beginners](https://www.youtube.com/watch?v=vtPkZShrvXQ)
* [Pluralsight - Spring Framework Fundamentals](https://app.pluralsight.com/library/courses/spring-framework-spring-fundamentals/table-of-contents)
* [Pluralsight - Spring Boot Fundamentals](https://app.pluralsight.com/library/courses/spring-boot-fundamentals/table-of-contents)


## License
[MIT](https://choosealicense.com/licenses/mit/)
