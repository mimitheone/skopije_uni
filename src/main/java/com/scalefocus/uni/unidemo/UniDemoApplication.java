package com.scalefocus.uni.unidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class UniDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniDemoApplication.class, args);
	}

}
