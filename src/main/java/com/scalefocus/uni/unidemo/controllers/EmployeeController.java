package com.scalefocus.uni.unidemo.controllers;

import com.scalefocus.uni.unidemo.models.Employee;
import com.scalefocus.uni.unidemo.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    //@GetMapping("/")
    //String sayHello(){
    //    return "Hello from Docker";
    //}

    @GetMapping("/employees")
    List<Employee> getAll() {
        return employeeService.getAll();
    }

    @PostMapping("/employees")
    Employee create(@RequestBody Employee newEmployee) {
        return employeeService.create(newEmployee);
    }

    @GetMapping("/employees/{id}")
    Employee getById(@PathVariable Long id) {
        return employeeService.getById(id);
    }

    @PutMapping("/employees/{id}")
    Employee update(@PathVariable Long id, @RequestBody Employee newEmployee) {
        return employeeService.update(id, newEmployee);
    }

    @DeleteMapping("/employees/{id}")
    void deleteById(@PathVariable Long id) {
        employeeService.deleteById(id);
    }
}