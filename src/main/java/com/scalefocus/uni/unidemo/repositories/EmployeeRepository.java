package com.scalefocus.uni.unidemo.repositories;

import com.scalefocus.uni.unidemo.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}