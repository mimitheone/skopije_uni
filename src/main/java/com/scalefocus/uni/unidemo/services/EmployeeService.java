package com.scalefocus.uni.unidemo.services;

import com.scalefocus.uni.unidemo.models.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAll();

    Employee create(Employee newEmployee);

    Employee getById(Long id);

    Employee update(Long id, Employee newEmployee);

    void deleteById(Long id);
}
