package com.scalefocus.uni.unidemo.services;

import com.scalefocus.uni.unidemo.EmployeeNotFoundException;
import com.scalefocus.uni.unidemo.models.Employee;
import com.scalefocus.uni.unidemo.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee create(Employee newEmployee) {
        return employeeRepository.save(newEmployee);
    }

    @Override
    public Employee getById(Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    @Override
    public Employee update(Long id, Employee newEmployee) {
        Optional<Employee> employeeOpt = employeeRepository.findById(id);

        if (employeeOpt.isEmpty()) {
            throw new EmployeeNotFoundException(id);
        }

        Employee employee = employeeOpt.get();
        employee.setName(newEmployee.getName());
        employee.setRole(newEmployee.getRole());

        return employeeRepository.save(employee);
    }

    @Override
    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }
}
